import requests
import os


def whstack_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv("WHSTACK_API_URL", default="https://whstack-api.example.com/")

    return settings.WHSTACK_API_URL


def whstack_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("WHSTACK_API_USER", default="user")
        password = os.getenv("WHSTACK_API_PASSWD", default="secret")
        return username, password

    username = settings.WHSTACK_API_USER
    password = settings.WHSTACK_API_PASSWD

    return username, password


def whstack_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{whstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def whstack_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{whstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def whstack_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{whstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def whstack_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{whstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def whstack_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{whstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def whstack_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{whstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def whstack_get_api_credentials_for_customer(mdat_id: int):
    username, password = whstack_default_credentials()

    if username != mdat_id:
        response = whstack_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def whstack_get_registrydomains(mdat_id: int):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        "v1/registrydomain",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def whstack_get_registrydomain(mdat_id: int, domain: str):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        f"v1/registrydomain/{domain}",
        username,
        password,
    )

    return response


def whstack_register_domain(mdat_id: int, domain: str, auth_code: str = None):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    payload = dict()

    payload["domain"] = domain

    if auth_code is not None:
        payload["auth_code"] = auth_code

    response = whstack_post_data(
        "v1/registrydomain",
        username,
        password,
        data=payload,
    )

    return response


def whstack_get_dnszones(mdat_id: int):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        "v1/dnszone",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def whstack_get_dnszone(mdat_id: int, zone: str):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        f"v1/dnszone/{zone}",
        username,
        password,
    )

    return response


def whstack_update_dnszone_record(
    mdat_id: int, zone: str, name: str, type: str, content: list
):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    fqdn = f"{name}.{zone}"

    payload = {
        "rrsets": [
            {
                "name": fqdn + ".",
                "type": type,
                "changetype": "REPLACE",
                "ttl": 3600,
                "records": [],
            }
        ]
    }

    for content_data in content:
        payload["rrsets"][0]["records"].append(
            {
                "content": content_data,
                "disabled": False,
                "name": fqdn + ".",
                "type": type,
                "priority": 0,
            }
        )

    response = whstack_patch_data(
        f"v1/dnszone/{zone}",
        username,
        password,
        data=payload,
    )

    return response


def whstack_get_dnszonetemplates(mdat_id: int):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        "v1/dnszonetemplate",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def whstack_get_dnszonetemplate(mdat_id: int, zone: str):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        f"v1/dnszonetemplate/{zone}",
        username,
        password,
    )

    return response


def whstack_get_websites(mdat_id: int):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        "v1/website",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def whstack_get_website(mdat_id: int, website_id: int):
    username, password = whstack_get_api_credentials_for_customer(mdat_id)

    response = whstack_get_data(
        f"v1/website/{website_id}",
        username,
        password,
    )

    return response
